<?php

/*
 * @author Loïc Gallay <lgallay@orange.fr>
 */

namespace App\Service;

use App\Helper\LoggerTrait;
use Http\Client\Exception;
use Nexy\Slack\Client;
use Psr\Log\LoggerInterface;

/**
 * Class SlackClient
 */
class SlackClient
{
    use LoggerTrait;

    /**
     * @var Client
     */
    private $slack;

    /**
     * @var LoggerInterface|null
     */
    private $logger;

    /**
     * SlackClient constructor.
     *
     * @param Client $slack
     */
    public function __construct(Client $slack)
    {
        $this->slack = $slack;
    }

    /**
     * @param string $from
     * @param string $message
     *
     * @throws Exception
     */
    public function sendMessage(string $from, string $message)
    {
        $this->logInfo('Beaming a message to Slack !', compact('message'));
        $message = $this->slack
            ->createMessage()
            ->from($from)
            ->withIcon(':ghost:')
            ->setText($message);
        $this->slack->sendMessage($message);
    }

    /**
     * @param LoggerInterface $logger
     *
     * @required
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
}